FROM python:slim

ADD json_run.py /app/json_run.py
ADD collector /etc/cron.d/collector
RUN chmod 0644 /etc/cron.d/collector
RUN touch /var/log/cron.log
RUN apt-get update
RUN apt-get -y install gcc cron
RUN apt-get -y install --reinstall build-essential
RUN pip3 install requests python-geohash influxdb Geohash
CMD ["cron", "-f"]
ENTRYPOINT ["tail", "-f", "/dev/null"]
