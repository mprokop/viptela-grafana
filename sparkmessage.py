#!/usr/bin/python
import urllib2, urllib
import json
import requests
import sys


auth_token = (sys.argv[0])
header = {'Authorization': 'Bearer ' + auth_token}

data = {
 	"roomId": (sys.argv[1]),
    "markdown": "**Status Update -   " + (sys.argv[2]) + "** \n\n"
}

url = 'https://api.ciscospark.com/v1/messages'
response = requests.post(url, json=data, headers=header)
print(response)
print(response.json())
