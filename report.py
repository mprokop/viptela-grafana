import requests
import json
import datetime
import time
import Geohash
from requests.auth import HTTPDigestAuth
from influxdb import InfluxDBClient
from ast import literal_eval

#Reading pass.txt with all user and password details. File added into .gitignore.
pass_file = open("./pass.txt","r") #opens file with name of "pass.txt"
fileList = pass_file.read().splitlines()
viptela_url = fileList[0]
viptela_user = fileList[1]
viptela_pass = fileList[2]
influxdb_ip = fileList[3]
influxdb_user = fileList[4]
influxdb_pass = fileList[5]


class viptelaApiCalls(object):
    def FabricStatus (self):
        #Define arrays
        msgList = []
        vedgeList = []
        
        #Auth token
        msgList.append("ZTkyMjdiNDMtOGIyYi00ZGFkLWFhMzAtNTUyN2Y5MDMyOWI0Y2UwMzRkZmItYWU5")
        
        #Rooom ID
        msgList.append('Y2lzY29zcGFyazovL3VzL1JPT00vNWNjNzBkOTAtYjIxMC0xMWU4LTg0ZGItYjFhNmY0NzAyN2Ew')

        #REST API requests
        alarmsCount = requests.get(str(viptela_url) + '/dataservice/alarms/count', auth=(viptela_user, viptela_pass), verify=False)
        overallNormal = requests.get(str(viptela_url) + '/dataservice/device?personality=vedge&status=normal', auth=(viptela_user, viptela_pass), verify=False)
        overallWarn = requests.get(str(viptela_url) + '/dataservice/device?personality=vedge&status=warning', auth=(viptela_user, viptela_pass), verify=False)
        overallError = requests.get(str(viptela_url) + '/dataservice/device?personality=vedge&status=error', auth=(viptela_user, viptela_pass), verify=False)
        
        #New Alarms in vManage
        alarmsCount = json.loads(alarmsCount.content)
        list_json = list(alarmsCount['data'])
        list_json = str(list_json[0])
        line_json = literal_eval(list_json)
        msgList.append(line_json.get('count'))
        
        #How many vEdge in normal state
        overallNormal = json.loads(overallNormal.content)
        normalCount = len(list(overallNormal['data']))
        msgList.append (normalCount)

        #How many vEdge in warning state
        overallWarn = json.loads(overallWarn.content)
        warnCount = len(list(overallWarn['data']))
        msgList.append (warnCount)

        #How many vEdge in error state
        overallError = json.loads(overallError.content)
        errorCount = len(list(overallError['data']))
        msgList.append (errorCount)        
        return msgList
    
    def SparkMessage(self, msgList):
      auth_token = (msgList[0])
      header = {'Authorization': 'Bearer ' + auth_token}
      data = {
        "roomId": (msgList[1]),
          "markdown": "**Fanatics - Morning Status Update** \n\n" +
          "New Alerts: " + str(msgList[2]) + "  \n" +
          "Sites in Normal State: " + str(msgList[3]) + "  \n" +
          "Sites in Warning State: " + str(msgList[4]) + "  \n" +
          "Sites in Error State: " + str(msgList[5]) + "  \n",
}
      url = 'https://api.ciscospark.com/v1/messages'
      response = requests.post(url, json=data, headers=header)

#Run all functions
viptelaCall = viptelaApiCalls()
msgList = viptelaCall.FabricStatus()
viptelaCall.SparkMessage(msgList)