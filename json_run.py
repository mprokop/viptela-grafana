#!/usr/bin/python

import requests
import json
import datetime
import time
import Geohash
import os
from requests.auth import HTTPDigestAuth
from influxdb import InfluxDBClient
from ast import literal_eval

viptela_url = os.environ.get('VMANAGE_URL')
viptela_user = os.environ.get('VMANAGE_USERNAME')
viptela_pass = os.environ.get('VMANAGE_PASSWORD')
influxdb_ip = os.environ.get('INFLUXDB_IP')
influxdb_user = os.environ.get('INFLUXDB_USERNAME')
influxdb_pass = os.environ.get('INFLUXDB_PASSWORD')



class viptelaApiCalls(object):
    def EdgeStatus (self):
		#GET request about status of all devices
        deviceStatsJson = requests.get(str(viptela_url) + '/dataservice/device', auth=(viptela_user, viptela_pass), verify=False)
        if(deviceStatsJson.ok):
         jData = json.loads(deviceStatsJson.content)
         #JSON response is in data. Not interested in header etc.
         list_json = list(jData['data'])
         #Build the complete list of all managed devices. Will returned by this function and used by other functions.
         sysIpList = []
         vedgeCloudList = []
         vedgeList = []
         for line in list_json:
          #print (line)
          #line_json will contain information about single device. Remove all additional quote because of inconsistent Viptela API
          line_json = str(line).replace('"', '')
          #Build the list of attributes from line_json
          line_json = literal_eval(line_json)
          #Add 'system-ip' into the list
          sysIpList.append(line_json.get('system-ip'))
          #GeoHash from lat/long
          geohash = Geohash.encode(float(line_json.get('latitude')), float(line_json.get('longitude')))
          #Build JSON req for influxdb
          json_body = [{ "measurement": "vedgeStatus", "tags": {"host-name": line_json.get('host-name')},"time": str(datetime.datetime.now()),"fields": {'reachability': int(line_json.get('reachability').replace('unreachable', '0').replace('reachable', '1')), 'longitude': float(line_json.get('longitude')), 'latitude': float(line_json.get('latitude')), 'geohash': str(geohash)}}]
          #Build edgeList
          if ((line_json.get('device-model') == "vedge-cloud") or (line_json.get('device-model') == "vmanage") or (line_json.get('device-model') == "vsmart")and line_json.get('reachability') == 'reachable'):
            vedgeCloudList.append (line_json.get('system-ip'))
          else:
            vedgeList.append (line_json.get('system-ip'))
          #Configure influx parameters
          client = InfluxDBClient(influxdb_ip, 8086, influxdb_user, influxdb_pass, 'viptela')
          #Send the request
          client.write_points(json_body)
          #print (json_body)
         return sysIpList,vedgeCloudList,vedgeList
        else:
  # If response code is not ok (200), print the resulting http error code with description
         deviceStatsJson.raise_for_status()

    def vedgeCloudIfaceStat(self, sysId):
        for line in sysId:
         #print (line)
         #GET request about device interfaces. 'line' will contain sysId of the device
         ifaceStatsJson = requests.get(str(viptela_url) + '/dataservice/device/interface/stats?deviceId='+line+'&af-type=ipv4&&&', auth=(viptela_user, viptela_pass), verify=False)
         #ifaceStatsJson = requests.get(str(viptela_url) + '/dataservice/device/interface?deviceId='+line, auth=(viptela_user, viptela_pass), verify=False)
         #print (ifaceStatsJson)
         jData = json.loads(ifaceStatsJson.content)
         list_json = list(jData['data'])
         #print (list_json)
         for line in list_json:
          line_json = str(line).replace('"', '')
          line_json = literal_eval(line_json)
          if "ge0" in line_json.get('ifname'):
              json_body = [{ "measurement": "vedgeInts", "tags": {"host-name": line_json.get('vdevice-host-name'), "interface": str(line_json.get('ifname'))},"time": str(datetime.datetime.now()),"fields": {'rx_kbps': int(line_json.get('rx-kbps')), 'tx_kbps': int(line_json.get('tx-kbps'))}}]
              client = InfluxDBClient(influxdb_ip, 8086, influxdb_user, influxdb_pass, 'viptela')
              client.write_points(json_body)
              print (json_body)
        else:
  # If response code is not ok (200), print the resulting http error code with description
         ifaceStatsJson.raise_for_status()

    def vedgeIfaceStat(self, sysId):
        for line in sysId:
         #print (line)
         #GET request about device interfaces. 'line' will contain sysId of the device
         ifaceStatsJson = requests.get(str(viptela_url) + '/dataservice/device/interface?deviceId='+line, auth=(viptela_user, viptela_pass), verify=False)
         #print (ifaceStatsJson)
         jData = json.loads(ifaceStatsJson.content)
         list_json = list(jData['data'])
         #print (list_json)
         for line in list_json:
          line_json = str(line).replace('"', '')
          line_json = literal_eval(line_json)
          if ("GigabitEthernet0/" in line_json.get('ifname')) and ("." not in line_json.get('ifname')):
              json_body = [{ "measurement": "vedgeInts", "tags": {"host-name": line_json.get('vdevice-host-name'), "interface": str(line_json.get('ifname'))},"time": str(datetime.datetime.now()),"fields": {'rx_kbps': int(line_json.get('rx-kbps')), 'tx_kbps': int(line_json.get('tx-kbps'))}}]
              client = InfluxDBClient(influxdb_ip, 8086, influxdb_user, influxdb_pass, 'viptela')
              client.write_points(json_body)
              print (json_body)
        else:
  # If response code is not ok (200), print the resulting http error code with description
         ifaceStatsJson.raise_for_status()


    def TunnelStats(self, sysId):
        for line in sysId:
         #GET request about device interfaces. 'line' will contain sysId of the device
         ifaceStatsJson = requests.get(str(viptela_url) + '/dataservice/device/app-route/statistics?deviceId='+line, auth=(viptela_user, viptela_pass), verify=False)
         jData = json.loads(ifaceStatsJson.content)
         list_json = list(jData['data'])
         for line in list_json:
           line_json = str(line).replace('"', '')
           line_json = literal_eval(line_json)
           if line_json.get('index') == "0":
            json_body = [{ "measurement": "tunnelStats", "tags": {"host-name": line_json.get('vdevice-host-name'), "tunnel-name": line_json.get('vdevice-host-name')+':'+line_json.get('local-color')+'-'+line_json.get('remote-system-ip')+':'+line_json.get('remote-color')}, "time": str(datetime.datetime.now()),"fields": {"mean-latency": int(line_json.get('average-latency')), "mean-loss": int(line_json.get('loss')), "average-jitter": int(line_json.get('average-jitter'))}}]
            print (json_body)
           client = InfluxDBClient(influxdb_ip, 8086, influxdb_user, influxdb_pass, 'viptela')
           client.write_points(json_body)
        else:
  # If response code is not ok (200), print the resulting http error code with description
         ifaceStatsJson.raise_for_status()

viptelaCall = viptelaApiCalls()
sysIpList,vedgeCloudList,vedgeList = viptelaCall.EdgeStatus()
viptelaCall.vedgeCloudIfaceStat(vedgeCloudList)
viptelaCall.vedgeIfaceStat(vedgeList)
viptelaCall.TunnelStats(vedgeCloudList)
viptelaCall.TunnelStats(vedgeList)
